
#ifndef _MIPS_H
#define _MIPS_H
#include <string>
#include <iostream>
#include <sstream>

#include "Instruction.h"
#include "InstData.h"
#include "Enum_type.h"
#include "Stages.h"
#include "Program.h"
#include "EmulProg.h"

class Program;
class InstData;
class Stages;
class EmulProg;

using namespace std;

class Mips
{
    public :
        Mips(Program * prog, int nb_I, int nb_D, int nb_E, int nb_M, int m_bypass, char calc_addr, int * branch);
        ~Mips();
        int do_cycle(void);
        void set_mips_stages(void);
        void display_InstData(void);
        void set_pc(int pc);
        void set_nextpc(int nextpc);
        int get_pc(void);
        int get_cycle(void);
        int get_branch(void);
        int get_nb_stages(void);
        int * get_prod_reg(void);
        int get_max_stage(char c);
        int get_min_stage(char c);
        void fill_cons_reg(int cons_reg[][2], int inst);
        int get_next_inst(void);
        int set_inst_stage(int inst, int stage);
        void shift_prod_reg(int * shift_tab);
        int check_end(void);
        int get_emul_inst(int stage_nb);
        Program * get_prog(void);
        EmulProg * get_emul_prog(void);
        int check_code(void);
        int check_arch_error(void);
    private :
        int nb_I_;
        int nb_D_;
        int nb_E_;
        int nb_M_;
        int m_bypass_;
        Program * prog_;
        Stages ** stages_;
        InstData ** Dinst_;
        EmulProg * emul_;
        int * branch_;
        int branchi_;
        int pc_;
        int cycle_;
        int nextpc_;
        int nb_insts_;
        int calc_addr_s_;
        // first half of prod reg contains the register number
        // second half of prod reg contains the number of stage at which the instruction will produce the register number
        int * prod_reg_;
        int nb_stages_;
};
#endif
