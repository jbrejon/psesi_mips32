
#ifndef SVG_H
#define SVG_H

#include <string>
#include <iostream>
#include <fstream>
#include "EmulProg.h"

class Svg
{
    private :
        std::fstream    sfile_;
        int             unit_size_x_;
        int             unit_size_y_;
        int             pixel_x_;
        int             pixel_y_;
        int             left_padding_;
        int             upper_padding_;
        int             width_;
        int             height_;
        int             cycle_;
        int             inst_;
    public :
        Svg(const std::string & filename, int cycle, int inst);
        ~Svg();
        void drawgrid(void);
        void drawstring(const std::string & s, int cycle, int line);
        void drawbox(int cycle, int line);
        void drawfreeze(int cycle, int line);
        void drawinst(const std::string & s, int line);
        void drawbypass(int cycle, int inst1, int inst2, int color); // TODO
        void output(EmulProg * emul);
};
#endif
