
#ifndef __INST_DATA_H
#define __INST_DATA_H
#include <string>
#include <iostream>
using namespace std;

enum t_Data     {ProdD = 0, ConsD, ProdE, ConsE, ProdM, ConsM, ProdW, NotAnOp, t_Data_size};
static string Data[t_Data_size]   = {"D", "D", "E", "E", "M", "M", "W", "X"};

class InstData
{
    public :
        InstData(string name, t_Data op1, t_Data op2, t_Data op3);
        ~InstData();

        void display(void);
        string get_name(void);
        string ** get_data(void);
    private :
        string name_;
        string ** dinst_;//[3][2]
};
#endif
