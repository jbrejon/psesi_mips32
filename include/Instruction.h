
#ifndef _INST_H
#define _INST_H
#include <string>
#include "Enum_type.h"
#include <iostream>
#include <sstream>
using namespace std;

class Instruction
{
    public :
        Instruction(t_Operator ope, int op1, int op2, int op3);
        Instruction(t_Operator ope, int op1, int op2);
        Instruction(t_Operator ope, int op1);
        Instruction(t_Operator ope);

        Instruction(t_Operator ope, int op1, int op2, string label);
        Instruction(t_Operator ope, int op1, string label);
        Instruction(t_Operator ope, string label);

        Instruction(t_Operator ope, int op1, string expr, int op3);

        ~Instruction();

        t_Operator getope(void);
        string get_label(void);
        int get_reg_number(int op_number); // 0 or 1 or 2;
        void display(void);
    private :
        t_Operator ope_;
        int op_[3];
        string label_;
        string expr_;
        int nb_op_;
};
#endif
