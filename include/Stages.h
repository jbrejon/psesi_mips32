
#ifndef _STAGES_H
#define _STAGES_H
#include <string>
#include <map>

#include "Mips.h"
using namespace std;

class Mips;

class Stages
{
    public:
        Stages(char * name, int bypass, int calc_addr, int stage_number);
        ~Stages();
        int verif_bypass(void);
        int exec(void);
        int shift(void);
        void set_mips(Mips * mips);
        int set_inst(int inst);
        int get_inst(void);
        int get_emul_inst(void);
    private:
        string name_;
        int instruction_; //instruction's id currently in the stage
        int bypass_;
        Mips * mips_;
        int inst_ready_;
        int calc_addr_;
        int stage_number_;
        int emul_inst_;
};
#endif

