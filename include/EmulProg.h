

#ifndef _EMUL_PROG_H
#define _EMUL_PROG_H
#define MAX_INST 30
#define MAX_CYCLE 30
#define MAX_BYPASS 30
#include <string>
#include <map>
using namespace std;
typedef struct bypass_s
{
    int cycle;
    int inst1;
    int inst2;
    int op;
}bypass_t;
class EmulProg
{
    public:
        EmulProg();
        ~EmulProg();
        int add_inst(string name);
        void add_data(string dat, int inst_number);
        int add_bypass(int cycle, int inst1, int inst2, int op);
        void inc_cycle(void);
        void display(void);
        int get_cycle(void);
        int get_inst(void);
        string ** get_data(void);
        string * get_insts(void);
        int get_nb_bypass(void);
        bypass_t * get_bypass(void);

    private:
        int nb_inst_;
        int nb_cycle_;
        int nb_bypass_;
        string * instructions_;
//        string data_[MAX_INST][MAX_CYCLE];
        string ** data_;
        bypass_t * bypass_;//cycle, inst1, inst2

};
#endif
