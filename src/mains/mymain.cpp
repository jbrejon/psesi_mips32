#include <iostream>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "Program.h"
#include "Mips.h"
#include "Svg.h"
#include "EmulProg.h"

int error(std::string mes)
{
    std::cout << mes << std::endl;
    exit(-1);
}

int main(int argc, char * argv[])
{
    std::string arg;
    std::string file = std::string("");
    int nb_i = 1;
    int nb_e = 1;
    int nb_d = 1;
    int nb_m = 1;
    int m_bypass = 0;
    char calc_addr = 'D';
    int check[8] = {0};
    int * branch_tab = new int[30];
    std::string output = std::string("output.svg");
    for (int i = 0; i < 30; i++)
        branch_tab[i] = 0;
    if (argc < 3)
    {
        std::cout << "Usage : " << argv[0] << " [options] -f <file>, --file <file>" << std::endl;
        std::cout << "Options :" << std::endl;
        std::cout << "-I <value>                           valid values : [1, 3]  Default : 1" << std::endl;
        std::cout << "-D <value>                           valid values : [1, 3]  Default : 1" << std::endl;
        std::cout << "-E <value>                           valid values : [1, 3]  Default : 1" << std::endl;
        std::cout << "-M <value>                           valid values : [1, 3]  Default : 1" << std::endl;
        std::cout << "-bt <value>, --branch-taken <value>  valid values : [0, 29] Default : no conditional branch is taken" << std::endl;
        std::cout << "--m-bypass <value>                   valid values : [0, 1]  Default : 0" << std::endl;
        std::cout << "--calc-addr <char>                   valid values : D, E    Default : D" << std::endl;
        std::cout << "-o <file>, --output <file>                                  Default : ./output.svg" << std::endl;
        return 0;
    }     
    else
    {
        for (int i = 1; i < argc - 1; i++)
        {
            if (i + 1 > argc - 1)
                error("Wrong arguments");
            arg = std::string(argv[i]);
            if (arg == "--file" || arg == "-f")
            {
                if (!check[0])
                {
                    file = std::string(argv[i+1]); 
                    check[0] = 1;
                }
                else
                    error("Error : file set multiple times");
            }
            else if (arg == "-I") 
            {
                if (!check[1])
                {
                    nb_i = atoi(argv[i + 1]);
                    check[1] = 1;
                    if (nb_i > 3 || nb_i < 1)
                        error("Error : stage size must be [1, 3] ");
                }
                else
                    error("Error : I set multiple times");
            }
            else if (arg == "-D") 
            {
                if (!check[2])
                {
                    nb_d = atoi(argv[i + 1]);
                    check[2] = 1;
                    if (nb_d > 3 || nb_d < 1)
                        error("Error : stage size must be [1, 3] ");
                }
                else
                    error("Error : D set multiple times");
            }
            else if (arg == "-E") 
            {
                if (!check[3])
                {
                    nb_e = atoi(argv[i + 1]);
                    check[3] = 1;
                    if (nb_e > 3 || nb_e < 1)
                        error("Error : stage size must be [1, 3] ");
                }
                else
                    error("Error : E set multiple times");
            }
            else if (arg == "-M") 
            {
                if (!check[4])
                {
                    nb_m = atoi(argv[i + 1]);
                    check[4] = 1;
                    if (nb_m > 3 || nb_m < 1)
                        error("Error : stage size must be [1, 3] ");
                }
                else
                    error("Error : M set multiple times");
            }
            else if (arg == "--m-bypass")
            {
                if (!check[5])
                {
                    m_bypass = atoi(argv[i + 1]);
                    check[5] = 1;
                    if (m_bypass != 0 && m_bypass != 1)
                        error("Error : m_bypass values must be 0 or 1");
                }
                else
                    error("Error : m_bypass set multiple times");
            }
            else if (arg == "--calc-addr")
            {
                if (!check[6])
                {
                    calc_addr = argv[i + 1][0];
                    check[6] = 1;
                    if (calc_addr != 'D' && calc_addr != 'E')
                        error("Error : calc_addr must be 'D' or 'E'");
                }
                else
                    error("Error : calc_addr set multiple times");
            }
            else if (arg == "-bt" || arg == "--branch-taken")
            {
                if (atoi(argv[i + 1]) >= 30 || atoi(argv[i + 1]) < 0)
                    error("Error : Branch must be [0, 30[");
                branch_tab[atoi(argv[i + 1])] = 1;
            }
            else if (arg == "-o" || arg == "--output")
            {
                if (!check[7])
                {
                    check[7] = 1;
                    output = argv[i + 1];
                }
                else
                    error("Error : output set multiple times");
            }
            else
                error("Error : " + arg + " is not a valid parameter");
            i++;
        }
    }

    if (file == "")
        error("Error : file must be set");

    struct stat buffer;
    if (!(stat(file.c_str(), &buffer) == 0))
        error("Error : file does not exist");

//    std::cout << "Configuration : " << std::endl;
//    std::cout << "File : "                 << file      << std::endl;
//    std::cout << "I : "                    << nb_i      << std::endl;
//    std::cout << "D : "                    << nb_d      << std::endl;
//    std::cout << "E : "                    << nb_e      << std::endl;
//    std::cout << "M : "                    << nb_m      << std::endl;
//    std::cout << "M bypass : "             << m_bypass  << std::endl;
//    std::cout << "Address calculation : "  << calc_addr << std::endl;
//    std::cout << "Output file : "          << output    << std::endl;


    Program prog(file);
//    prog.display();
    Mips * mips = new Mips(&prog, nb_i, nb_d, nb_e, nb_m, m_bypass, calc_addr, branch_tab);
    mips->set_mips_stages();
    mips->do_cycle();
    EmulProg * emul = mips->get_emul_prog();
    Svg * drawing = new Svg(output, emul->get_cycle(), emul->get_inst());
    drawing->output(emul);
    delete drawing;
    return 0;
}
