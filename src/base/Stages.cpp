#include "Stages.h"
#include "Mips.h"

Stages::Stages(char * name, int bypass, int calc_addr, int stage_number)
{
    name_ = std::string(name);
    bypass_ = bypass;
    instruction_ = -1;
    inst_ready_ = 0;
    calc_addr_ = calc_addr;
    stage_number_ = stage_number;
    emul_inst_ = -1;
}
int Stages::verif_bypass(void)
{
    if (instruction_ == -1)
        return 0;
    if (bypass_ == 0)
    {
        inst_ready_ = 1;
        return 0;
    }
    int * prod_reg = mips_->get_prod_reg();
    int nb_stages = mips_->get_nb_stages(); 
    int cons_reg[2][2] = { { -1, -1 }, { -1, -1} };
    /* ####     cons reg    #######
       #        0           1     #[][x]
       # 0 |nb_reg_op1|nb_reg_op2|#
       # 1 |cons_stage|cons_stage|# 
       #[y][]                     #
       ############################*/
    mips_->fill_cons_reg(cons_reg, instruction_);
    int ok = -1;
    int mflohi = 0;
    int done[2] = {0 ,0};
    if (cons_reg[0][0] == 32 || cons_reg[0][0] == 33) // mfhi or mflo
        mflohi = 1;
    
    for (int i = stage_number_ + 1; i < nb_stages; i++)
    {
        if (cons_reg[0][0] > 0 && done[0] == 0)
        {
            if (prod_reg[i] == cons_reg[0][0] && stage_number_ <= cons_reg[1][0])
            {
                if (i >= prod_reg[i + nb_stages] + 1)
                {
                    (mips_->get_emul_prog())->add_bypass(mips_->get_cycle(), emul_inst_, mips_->get_emul_inst(i), 0);
                    if (ok == -1)
                    ok = 1;
                }
                else if (cons_reg[1][0] > stage_number_) 
                {
                    if (ok == -1)
                        ok = 1;
                }
                else
                    ok = 0;
                done[0] = 1;
            }
            else if (mflohi && prod_reg[i] == 34)
            {
                if (mips_->get_min_stage('E') > stage_number_)
                    ok = 1;
                else
                    ok = 0;
                done[0] = 1;
            }
        }
        if (cons_reg[0][1] > 0 && done[1] == 0)
        {
            if (prod_reg[i] == cons_reg[0][1] && stage_number_ <= cons_reg[1][1])
            {
                if (i >= prod_reg[i + nb_stages] + 1)
                {
                    (mips_->get_emul_prog())->add_bypass(mips_->get_cycle(), emul_inst_, mips_->get_emul_inst(i), 1);
                    if (ok == -1)
                        ok = 1;
                    done[1] = 1;
                }
                else if (cons_reg[1][1] > stage_number_) 
                {
                    if (ok == -1)
                        ok = 1;
                    done[1] = 1;
                }
                else
                {
                    ok = 0;
                    done[1] = 1;
                }
            }
        }
    } 
    if (ok == 1 || ok == -1)
        inst_ready_ = 1;
    else
        inst_ready_ = 0;
    return 0;
}
int Stages::exec(void)
{
    if (instruction_ == -1)
        return 0;
    // Branchement handling
    string inst_name = op_profile[((mips_->get_prog())->get_instructions())[instruction_]->getope()].nom;
    if (calc_addr_ == 1 && (inst_name == "j" || inst_name == "jal"))
        mips_->set_nextpc((mips_->get_prog())->get_label(((mips_->get_prog())->get_instructions())[instruction_]->get_label()));
    else if (calc_addr_ == 1 && op_profile[((mips_->get_prog())->get_instructions())[instruction_]->getope()].type == BR && inst_ready_ == 1 && mips_->get_branch())
        mips_->set_nextpc((mips_->get_prog())->get_label(((mips_->get_prog())->get_instructions())[instruction_]->get_label()));
    return 1;

}
int Stages::shift(void) //W first
{
    if (name_ == "I" || name_ == "I0")
    {
        if (mips_->set_inst_stage(instruction_, stage_number_ + 1) == -1)
            return -1;
        if (instruction_ != -1)
            (mips_->get_emul_prog())->add_data(name_, emul_inst_);
        instruction_ = mips_->get_next_inst();
        if (instruction_ != -1)
            emul_inst_++;
        inst_ready_ = 0;
        return 0;
    }
    else if (name_ == "W")
    {
        if (instruction_ != -1)
            (mips_->get_emul_prog())->add_data(name_, emul_inst_);
        instruction_ = -1;
        inst_ready_ = 0;
        return 0;
    }
    else
    {
        if (instruction_ != -1)
        {
            if (inst_ready_ == 0)
            {
                (mips_->get_emul_prog())->add_data("C", emul_inst_);
                return -1;
            }
        }
        if (mips_->set_inst_stage(instruction_, stage_number_ + 1) == -1)
        {
            inst_ready_ = 0;
            if (instruction_ != -1)
                (mips_->get_emul_prog())->add_data("C", emul_inst_);
            return -1;
        }
        if (instruction_ != -1)
            (mips_->get_emul_prog())->add_data(name_, emul_inst_);
        instruction_ = -1;
        inst_ready_ = 0;
        return 0;
    }
    return 0;
}

int Stages::set_inst(int inst)
{
    if (instruction_ != -1)
        if (inst_ready_ == 0)
            return -1;
    instruction_ = inst;
    if (instruction_ != -1)
        emul_inst_++;
    return 0;
}

int Stages::get_inst(void)
{
    return instruction_;
}

int Stages::get_emul_inst(void)
{
    return emul_inst_;
}

void Stages::set_mips(Mips * mips)
{
    mips_ = mips;
}
// private:
//         string name_;
//     int instruction_; //instruction's id currently in the stage
//     int produced_; 
//     int bypass_;
//     Mips * mips_;
//     int inst_ready_;


