#include "Program.h"
#include "Instruction.h"
#include "asm_mipsyac.h"
#include <iostream>
#include "Enum_type.h"

extern void programparse(string) ; 
extern Program prog ;
using namespace std;

Program::Program()
{
   length_ = 0;
   errors_ = 0;
   latest_line_is_label_ = 0;
   latest_label_ = std::string("");
}

Program::Program (string const file)
{
   programparse(file);
   length_ = prog.length_;
   errors_ = prog.errors_;
   for (int i = 0; i < length_; i++)
   {
       insts_[i] = prog.insts_[i];
   }
   labels_ = prog.labels_;
   inval_labels_ = prog.inval_labels_;
   
}

Program::~Program()
{
}

Instruction ** Program::get_instructions(void)
{
    return insts_;
}

int Program::check_labels(void)
{
    int found = 0;
    for (int i = 0; i < length_; i++)
    {
        if (insts_[i]->get_label() != "")
        {
            map<string, int>::iterator itmap = labels_.begin();
            for (; itmap != labels_.end(); itmap++)
            {
                if (insts_[i]->get_label() == itmap->first)
                    found = 1;
            }
            itmap = inval_labels_.find(insts_[i]->get_label());
            if (itmap != inval_labels_.end())
            {
                std::cout << "Error : Label " << insts_[i]->get_label() << " doesn't reference an instruction" << std::endl;
                return 0;
            }
            if (found == 0)
            {
                std::cout << "Error : Label " << insts_[i]->get_label() << " doesn't exists" << std::endl;
                return 0;
            }
        }
    }
    map<string, int>::iterator itmap;
    for(itmap = labels_.begin(); itmap != labels_.end(); itmap++)
    {
        if (inval_labels_.find(itmap->first) != inval_labels_.end())
        {
            std::cout << "Error : Multiple definition of label " << itmap->first << std::endl;
            return 0;
        }
    }
    return 1;
}

void Program::add_line(string label)
{
    latest_line_is_label_ = 1;
    latest_label_ = label;
    if (labels_.insert(pair<string, int>(label, length_)).second == false)
    {
        errors_++;
        std::cout << "label " << label << " already exsits" << std::endl;
    }
}

void Program::add_line(Instruction * inst)
{
    latest_line_is_label_ = 0;
    insts_[length_] = inst;
    length_++;
}

void Program::inval_label(void)
{
    if (latest_line_is_label_ == 1)
    {
        std::map<string, int>::iterator itmap;
        itmap = labels_.find(latest_label_);
        inval_labels_.insert(pair<string, int>(itmap->first, itmap->second));
        latest_line_is_label_ = 0;
    }
}

int Program::get_label(string s)
{
    map<string, int>::iterator itmap = labels_.find(s);
    return itmap->second;
}

int Program::get_nb_insts(void)
{
    return length_;
}

void Program::display(void)
{
    cout << length_ << endl;
    for (int i = 0; i < length_; i++)
    {
        insts_[i]->display();
    }

    std::cout << std::endl << "Labels" << std::endl;
    map<string, int>::const_iterator iter;
    for(iter = labels_.begin(); iter != labels_.end(); iter++)
        std::cout << iter->first << "-> line : " << iter->second << std::endl;
}


