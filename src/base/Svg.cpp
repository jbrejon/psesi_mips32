
#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include "Svg.h"

#define MIN(x, y) ((x) < (y) ? (x) : (y))

#define UNIT_PER_PIXEL 2
#define BASE_PIXEL_X 40
#define BASE_PIXEL_Y 30
#define RESOL_COEF 1.2
#define PIXEL_LEFT_PAD 80
#define PIXEL_UP_PAD 20


Svg::Svg(const std::string & filename, int cycle, int inst)
{
    char buf[200] = {' '};
    std::memset(buf, ' ', 200);
    buf[199] = '\0';
    unit_size_x_ = BASE_PIXEL_X * UNIT_PER_PIXEL;
    unit_size_y_ = BASE_PIXEL_Y * UNIT_PER_PIXEL;
    left_padding_ = PIXEL_LEFT_PAD * UNIT_PER_PIXEL;
    upper_padding_ = PIXEL_UP_PAD * UNIT_PER_PIXEL;
    cycle_ = cycle;
    inst_  = inst;
    width_ = left_padding_ + unit_size_x_ * cycle;
    height_ = upper_padding_ + unit_size_y_ * (inst + 1);
    pixel_x_ = RESOL_COEF * (PIXEL_LEFT_PAD + BASE_PIXEL_X * cycle);
    pixel_y_ = RESOL_COEF * (BASE_PIXEL_Y * (inst_ + 1));
    sfile_.open(filename.c_str(), std::fstream::out | std::ios::trunc);
    sfile_ << "<?xml version=\"1.0\" standalone=\"no\"?> " << std::endl;
    sfile_ << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" << std::endl;
    sfile_ << "<svg width=\"" << pixel_x_ << "px\" height=\"" << pixel_y_ << "px\" viewBox=\"0 0 " << width_ << " " << height_ << "\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">" << std::endl;
    sfile_ << buf << std::endl;
}

Svg::~Svg()
{
    sfile_ << "</svg>" << std::endl;
    sfile_.close();
}

void Svg::drawgrid(void)
{
    char cycleinchar[3]; 
    int cycle = cycle_;
    int line = 0;
    int nb_line = inst_;
    int nb_cycle = cycle;
    int x1, y1, x2, y2;
    for (line = 1; line < inst_; line = line + 2)
    {
        x1 = left_padding_;
        y1 = line * unit_size_y_ + upper_padding_;
        x2 = unit_size_x_ * nb_cycle;
        sfile_ << "<rect x=\"" << x1 << "\" y=\"" << y1 << "\" width=\"" << x2 << "\" height=\"" << unit_size_y_ << "\" fill=\"#F0F0F0\" fill-opacity=\"0.5\" stroke-width=\"0\"/>";
    }
    for (; cycle > 0; cycle--)
    {
        x1 = left_padding_ + (cycle - 1) * unit_size_x_;
        y1 = upper_padding_;
        x2 = x1;
        y2 = unit_size_y_ * (nb_line + 1) + upper_padding_; 
        sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\" x2=\"" << x2 << "\" y2=\"" << y2 << "\"";
        sfile_ << " stroke=\"#808080\" stroke-width=\"1\" stroke-dasharray=\""<< unit_size_y_ / 4 << " " << unit_size_y_ / 4 << "\"/>" << std::endl;
    }
    for (cycle = 1; cycle <= nb_cycle; cycle++)
    {
        sprintf(cycleinchar, "%d", cycle);
        drawstring(cycleinchar, cycle, nb_line + 1);
    }
    x1 = left_padding_; 
    y1 = upper_padding_ + nb_line * unit_size_y_; 
    x2 = left_padding_ + nb_cycle * unit_size_x_;
    y2 = y1;
    sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\" x2=\"" << x2 << "\" y2=\"" << y2 << "\"";
    sfile_ << " stroke=\"black\" stroke-width=\"1\" stroke-opacity=\"1.0\"/>" << std::endl;
    sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\" x2=\"" << x2 << "\" y2=\"" << y2 << "\"";
    sfile_ << " stroke=\"black\" stroke-width=\"1\" stroke-opacity=\"1.0\"/>" << std::endl;

}

void Svg::drawstring(const std::string & s, int cycle, int line)
{
//    int len = strlen(s.c_str());
    int px = 15 * UNIT_PER_PIXEL;
    int x = ((left_padding_ + cycle * unit_size_x_) - unit_size_x_ / 2);
    int y = ((upper_padding_ + line * unit_size_y_) - unit_size_y_ / 2) + px / 2;
    sfile_ << "<text x=\"" << x << "\" y=\"" << y << "\" font-size=\"" << px;
    sfile_ << "px\" font-style=\"italic\" text-anchor=\"middle\" fill=\"#000000\" fill-opacity=\"1\" font-family=\"Monospace\">" << s << "</text>" << std::endl;

}

void Svg::drawbox(int cycle, int line)
{
    int x = left_padding_ + (cycle - 1) * unit_size_x_;
    int y = upper_padding_ + (line - 1) * unit_size_y_;
    sfile_ << "<rect x=\"" << x << "\" y=\"" << y << "\" width=\"" << unit_size_x_ << "\" height=\"" << unit_size_y_ << "\" fill=\"none\" stroke=\"black\" stroke-opacity=\"1.0\"/>" << std::endl;
    sfile_ << "<rect x=\"" << x << "\" y=\"" << y << "\" width=\"" << unit_size_x_ << "\" height=\"" << unit_size_y_ << "\" fill=\"none\" stroke=\"black\" stroke-opacity=\"1.0\"/>" << std::endl;
}

void Svg::drawinst(const std::string & s, int line)
{
    int px = 15 * UNIT_PER_PIXEL;
    int x = left_padding_ / 2;
    int y = ((upper_padding_ + line * unit_size_y_) - unit_size_y_ / 2) + px / 2;
    sfile_ << "<text x=\"" << x << "\" y=\"" << y << "\" font-size=\"" << px;
    sfile_ << "px\" font-style=\"italic\" text-anchor=\"middle\" fill=\"#000000\" fill-opacity=\"1\" font-family=\"Monospace\">" << s << "</text>" << std::endl;
}

void Svg::drawfreeze(int cycle, int line)
{
    int x = left_padding_ + unit_size_x_ * cycle - unit_size_x_ / 2;
    int y = upper_padding_ + unit_size_y_ * line - unit_size_y_ / 2;
    int r = MIN(unit_size_x_, unit_size_y_) / 4;
    sfile_ << "<circle cx=\"" << x << "\" cy=\"" << y << "\" r=\"" << r << "\" stroke=\"black\" fill=\"#FEDCBA\"/>" << std::endl;
}

void Svg::drawbypass(int cycle, int inst1, int inst2, int color)
{
    int x = left_padding_ + unit_size_x_ * cycle;
    int y = upper_padding_ + unit_size_y_ * inst1 - unit_size_y_ / 2;
    int y2 = upper_padding_ + unit_size_y_ * inst2 - unit_size_y_ / 2;
    int r = MIN(unit_size_x_, unit_size_y_) / 8;
    std::string color_c = std::string("red");
    if (color == 1)
        color_c = std::string("lime");
    if (color == 2)
        color_c = std::string("blue");

    sfile_ << "<circle cx=\"" << x + 3 * r - 2 << "\" cy=\"" << y << "\" r=\"" << r << "\" fill=\"" << color_c << "\"/>" << std::endl;
    sfile_ << "<circle cx=\"" << x + r << "\" cy=\"" << y2 << "\" r=\"" << r << "\" fill=\"" << color_c << "\"/>" << std::endl;
    sfile_ << "<line x1=\"" << x + 2 * r - 1 << "\" y1=\"" << y << "\" x2=\"" << x + 2 * r << "\" y2=\"" << y2 << "\"";
    sfile_ << " stroke=\"" << color_c << "\" stroke-width=\"1.5\" stroke-opacity=\"1.0\"/>" << std::endl;
    sfile_ << "<line x1=\"" << x + 2 * r - 1 << "\" y1=\"" << y << "\" x2=\"" << x + 2 * r << "\" y2=\"" << y2 << "\"";
    sfile_ << " stroke=\"" << color_c << "\" stroke-width=\"1.5\" stroke-opacity=\"1.0\"/>" << std::endl;
}

void Svg::output(EmulProg * emul)
{
    string * insts = emul->get_insts();
    string ** data = emul->get_data();
    int nb_cycle = cycle_;
    int nb_insts = inst_;
    int bypass_color[nb_cycle];
    int nb_bypass = emul->get_nb_bypass();
    bypass_t * bypass = emul->get_bypass();
    drawgrid();
    for (int i = 0; i < nb_insts; i++)
        drawinst(insts[i], i + 1);
    for (int i = 0; i < nb_insts; i++)
    {
        for (int j = 0; j < nb_cycle; j++)
        {
            if (data[i][j] != "")
            {
                if (data[i][j] == "C")
                {
                    drawbox(j + 1, i + 1);
                    drawfreeze(j + 1, i + 1);
                }
                else
                {
                    drawbox(j + 1, i + 1);
                    drawstring(data[i][j], j + 1, i + 1);
                }
            }
        }
    }

    for (int i = 0; i < nb_cycle; i++)
        bypass_color[i] = 0;
    for (int i = nb_bypass - 1; i > -1; i--)
        drawbypass(bypass[i].cycle - 1, bypass[i].inst1 + 1, bypass[i].inst2 + 1, bypass_color[bypass[i].cycle]++ % 3);
}

