#include <stdlib.h>
#include <stdio.h>

#include "Mips.h"
#include "Stages.h"

#define NB_INST_MAX_ 100
#define MAX_BRANCH   30


Mips::Mips(Program * prog, int nb_I, int nb_D, int nb_E, int nb_M, int m_bypass, char calc_addr, int * branch)
{
    int i = 0;
    nb_I_ = nb_I;
    nb_D_ = nb_D;
    nb_E_ = nb_E;
    nb_M_ = nb_M;
    m_bypass_ = m_bypass;
    cycle_ = 0;
    int calc_addr_s = get_max_stage(calc_addr);
    calc_addr_s_ = calc_addr_s;
    nb_stages_ = nb_I_ + nb_D_ + nb_E_ + nb_M_ + 1;
    emul_ = new EmulProg();
    prog_ = prog;
    branch_ = branch;
//    for (int i = 0; i < MAX_BRANCH; i++)
//        std::cout << branch_[i];
//    std::cout << std::endl;
    branchi_ = 0;



    Dinst_ = new InstData *[NB_INST_MAX_];
    Dinst_[i++] = new InstData("add", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("sub", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("addu", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("subu", ProdE, ConsE, ConsE);

    Dinst_[i++] = new InstData("addi", ProdE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("addiu", ProdE, ConsE, NotAnOp);

    Dinst_[i++] = new InstData("or", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("and", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("xor", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("nor", ProdE, ConsE, ConsE);

    Dinst_[i++] = new InstData("ori", ProdE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("andi", ProdE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("xori", ProdE, ConsE, NotAnOp);

    Dinst_[i++] = new InstData("sllv", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("srlv", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("srav", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("sll", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("srl", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("sra", ProdE, ConsE, ConsE);

    Dinst_[i++] = new InstData("lui", ProdE, NotAnOp, NotAnOp);

    Dinst_[i++] = new InstData("slt", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("sltu", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("slti", ProdE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("sltiu", ProdE, ConsE, NotAnOp);

    Dinst_[i++] = new InstData("mult", ConsE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("multu", ConsE, ConsE, NotAnOp); 
    Dinst_[i++] = new InstData("div", ConsE, ConsE, NotAnOp); 
    Dinst_[i++] = new InstData("divu", ConsE, ConsE, NotAnOp); 

    Dinst_[i++] = new InstData("mfhi", ProdE, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("mflo", ProdE, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("mthi", ConsE, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("mtlo", ConsE, NotAnOp, NotAnOp);

    Dinst_[i++] = new InstData("lw", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("lh", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("lhu", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("lb", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("lbu", ProdM, NotAnOp, ConsE);

    if (m_bypass_)
    {
        Dinst_[i++] = new InstData("sw", ConsM, NotAnOp, ConsE);
        Dinst_[i++] = new InstData("sh", ConsM, NotAnOp, ConsE);
        Dinst_[i++] = new InstData("sb", ConsM, NotAnOp, ConsE);
    }
    else
    {
        Dinst_[i++] = new InstData("sw", ConsE, NotAnOp, ConsE);
        Dinst_[i++] = new InstData("sh", ConsE, NotAnOp, ConsE);
        Dinst_[i++] = new InstData("sb", ConsE, NotAnOp, ConsE);
    }

    if (calc_addr == 'D')
    {
        Dinst_[i++] = new InstData("beq", ConsD, ConsD, NotAnOp);
        Dinst_[i++] = new InstData("bne", ConsD, ConsD, NotAnOp);

        Dinst_[i++] = new InstData("bgez",   ConsD, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("bgtz",   ConsD, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("blez",   ConsD, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("bltz",   ConsD, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("bgezal", ConsD, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("bltzal", ConsD, NotAnOp, NotAnOp);
    }
    else
    {
        Dinst_[i++] = new InstData("beq", ConsE, ConsE, NotAnOp);
        Dinst_[i++] = new InstData("bne", ConsE, ConsE, NotAnOp);

        Dinst_[i++] = new InstData("bgez",   ConsE, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("bgtz",   ConsE, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("blez",   ConsE, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("bltz",   ConsE, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("bgezal", ConsE, NotAnOp, NotAnOp);
        Dinst_[i++] = new InstData("bltzal", ConsE, NotAnOp, NotAnOp);
    }

    Dinst_[i++] = new InstData("j", NotAnOp, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("jal", NotAnOp, NotAnOp, NotAnOp);

    nb_insts_ = i;

    // jr jalr : not supported.

    // Stages creation


    prod_reg_ = new int[nb_stages_ * 2];
    for (int i = 0; i < nb_stages_ * 2; i++)
        prod_reg_[i] = -1;
    char stage_name[3];
    stage_name[3] = '\0';
    int iter_stage;
    stages_ = new Stages*[nb_stages_];
    i = 0;
    int additional_stage = 0;
    for (int j = 0; j < 5; j++)
    {
        iter_stage = (j == 0)? nb_I_ :iter_stage;
        iter_stage = (j == 1)? nb_D_ :iter_stage;
        iter_stage = (j == 2)? nb_E_ :iter_stage;
        iter_stage = (j == 3)? nb_M_ :iter_stage;
        iter_stage = (j == 4)? 1     :iter_stage;

        stage_name[0] = (j == 0)? 'I' : stage_name[0];
        stage_name[0] = (j == 1)? 'D' : stage_name[0];
        stage_name[0] = (j == 2)? 'E' : stage_name[0];
        stage_name[0] = (j == 3)? 'M' : stage_name[0];
        stage_name[0] = (j == 4)? 'W' : stage_name[0];
        while (i != iter_stage)
        {
            if (iter_stage > 1)
                sprintf(&stage_name[1], "%d", i);
            else
                stage_name[1] = '\0';
            // bypasses adjustement
            if (j == 3) // M
                stages_[i + j + additional_stage] = new Stages(stage_name, m_bypass_, 0, i + j + additional_stage); 
            else if (j == 4 || j == 0) // I and W
                stages_[i + j + additional_stage] = new Stages(stage_name, 0, 0, i + j + additional_stage); 
            else if ((j + i + additional_stage == calc_addr_s) && (i == iter_stage - 1))// last substage of the stage which compute addr
                stages_[i + j + additional_stage] = new Stages(stage_name, 1, 1, i + j + additional_stage); 
            else // D and E
                stages_[i + j + additional_stage] = new Stages(stage_name, 1, 0, i + j + additional_stage);
            i++;
        }
        additional_stage += (i - 1);
        i = 0;
    }
}


int Mips::check_arch_error(void)
{
    Instruction ** inst = prog_->get_instructions();
    int nb_lines = prog_->get_nb_insts();
    int delayed_slot = 0;
    int inst_type;
    for (int i = 0; i < nb_lines; i++)
    {
        inst_type = op_profile[(inst[i])->getope()].type;
        if (inst_type == BR)
        {
            if (delayed_slot)
            {
                std::cout << "Error : Branchement (instruction number" << i + 1 << ") in delayed slot(s)... " << calc_addr_s_ << " delayed slot(s)" << std::endl;
                return 0;
            }
            else
                delayed_slot = calc_addr_s_;
        }
        else
        {
            if (delayed_slot != 0)
                delayed_slot--;
        }
    }
    if (delayed_slot)
    {
        std::cout << "Error : Empty delayed slot(s)... " << calc_addr_s_ << " delayed slot(s)" << std::endl;
        return 0;
    }
    return 1;
}

void Mips::set_mips_stages(void)
{
    for (int i = 0; i < nb_stages_ ; i++)
        stages_[i]->set_mips(this);
}

void Mips::display_InstData(void)
{
    for(int i = 0; i < nb_insts_ ; i++)
        Dinst_[i]->display();
}

void Mips::set_pc(int pc)
{
    pc_ = pc;
}

void Mips::set_nextpc(int nextpc)
{
    nextpc_ = nextpc;
}

int Mips::get_branch(void)
{
    return branch_[branchi_++];
}

int Mips::get_pc(void)
{
    return pc_;
}

int * Mips::get_prod_reg(void)
{
    return prod_reg_;
}

int Mips::get_nb_stages(void)
{
    return nb_stages_;
}

int Mips::get_max_stage(char c)
{
    if (c == 'I')
        return nb_I_ - 1;
    if (c == 'D')
        return nb_I_ + nb_D_ - 1;
    if (c == 'E')
        return nb_I_ + nb_D_ + nb_E_ - 1;
    if (c == 'M')
        return nb_I_ + nb_D_ + nb_E_ + nb_M_ - 1;
    if (c == 'W')
        return nb_I_ + nb_D_ + nb_E_ + nb_M_;
    return -1;
}

int Mips::get_min_stage(char c)
{
    if (c == 'I')
        return 0;
    if (c == 'D')
        return nb_I_;
    if (c == 'E')
        return nb_I_ + nb_D_;
    if (c == 'M')
        return nb_I_ + nb_D_ + nb_E_;
    if (c == 'W')
        return nb_I_ + nb_D_ + nb_E_ + nb_M_;
    return -1;
}

void Mips::fill_cons_reg(int cons_reg[][2], int inst_nb)
{
    Instruction * instr = (prog_->get_instructions())[inst_nb];
    string inst_name = op_profile[instr->getope()].nom;
    int (Mips::*func_ptr)(char c) = &Mips::get_min_stage;
    if ((inst_name == "sw" || inst_name == "sh" || inst_name == "sb") && !m_bypass_)
        func_ptr = &Mips::get_max_stage;
    if (inst_name == "mflo")
    {
        cons_reg[0][0] = 32; // LO
        cons_reg[1][0] = get_min_stage('E');
        return;
    }
    if (inst_name == "mfhi")
    {
        cons_reg[0][0] = 33; //  HI
        cons_reg[1][0] = get_min_stage('E');
        return;
    }
    string ** data;
    for (int i = 0; i < nb_insts_; i++)
    {
        if (inst_name == Dinst_[i]->get_name())
        {
            data = Dinst_[i]->get_data();
            break;
        }
    }
    int j = 0;
    if (data[0][1] != "" && data[0][1] != "X")
    {
        cons_reg[0][j] = instr->get_reg_number(0);
        cons_reg[1][j] = (*this.*func_ptr)(data[0][1].at(0));// only on the first 
        j++;
    }
    if (data[1][1] != "" && data[1][1] != "X")
    {
        cons_reg[0][j] = instr->get_reg_number(1);
        cons_reg[1][j] = get_min_stage(data[1][1].at(0)); 
        j++;
    }
    if (data[2][1] != "" && data[2][1] != "X")
    {
        cons_reg[0][j] = instr->get_reg_number(2);
        cons_reg[1][j] = get_min_stage(data[2][1].at(0)); 
        j++;
    }
}

int Mips::get_next_inst(void)
{
    pc_++;
    int done = 0;
    if (nextpc_ != -1)
    {
        pc_ = nextpc_;
        nextpc_ = -1;
    }
    if (pc_ >= prog_->get_nb_insts())
        return -1;
    if (op_profile[(prog_->get_instructions()[pc_])->getope()].nom == "add")
    {
        if ((prog_->get_instructions()[pc_])->get_reg_number(0) == 0)
        {
            if ((prog_->get_instructions()[pc_])->get_reg_number(1) == 0)
            {
                if ((prog_->get_instructions()[pc_])->get_reg_number(2) == 0)
                {
                    emul_->add_inst("nop");
                    done = 1;
                }
            }
        }

    }
    if (!done)
        emul_->add_inst(op_profile[(prog_->get_instructions()[pc_])->getope()].nom);
    return pc_;
}

int Mips::set_inst_stage(int inst, int stage)
{
    return stages_[stage]->set_inst(inst);
}

EmulProg * Mips::get_emul_prog(void)
{
    return emul_;
}

void Mips::shift_prod_reg(int * shift_tab)
{
    for (int i = nb_stages_ - 1; i > 0; i--)
    {
        if (shift_tab[i - 1] == -1)
        {
            prod_reg_[i] = -1;
            prod_reg_[i + nb_stages_] = -1;
            return;
        }
        prod_reg_[i] = prod_reg_[i - 1];
        prod_reg_[i + nb_stages_] = prod_reg_[i + nb_stages_ - 1];
    }
    if (pc_ >= prog_->get_nb_insts())
    {
        prod_reg_[0] = -1;
        prod_reg_[0 + nb_stages_] = -1;
        return;
    }

    Instruction * instr = (prog_->get_instructions())[pc_];
    string inst_name = op_profile[instr->getope()].nom;
    string ** data;
    if (inst_name == "jal" || inst_name == "bgezal" || inst_name == "bltzal")
    {
        prod_reg_[0] = 31;
        prod_reg_[0 + nb_stages_] = calc_addr_s_;
        return;
    }
    if (inst_name == "mtlo")
    {
        prod_reg_[0] = 32;
        prod_reg_[0 + nb_stages_] = get_max_stage('D');
        return;
    }
    if (inst_name == "mthi")
    {
        prod_reg_[0] = 33;
        prod_reg_[0 + nb_stages_] = get_max_stage('D');
        return;
    }
    if (inst_name == "div" || inst_name == "divu" || inst_name == "mult" || inst_name == "multu")
    {
        prod_reg_[0] = 34;
        prod_reg_[0 + nb_stages_] = 'W';
        return;
    }
    for (int i = 0; i < nb_insts_; i++)
    {
        if (inst_name == Dinst_[i]->get_name())
        {
            data = Dinst_[i]->get_data();
            break;
        }
    }
    if (data[0][0] != "" && data[0][0] != "X")
    {
        prod_reg_[0] = instr->get_reg_number(0);
        prod_reg_[0 + nb_stages_] = get_max_stage(data[0][0].at(0)); 
    }
    else if (data[1][0] != "" && data[1][0] != "X")
    {
        prod_reg_[0] = instr->get_reg_number(1);
        prod_reg_[0 + nb_stages_] = get_max_stage(data[1][0].at(0)); 
    }
    else if (data[2][0] != "" && data[2][0] != "X")
    {
        prod_reg_[0] = instr->get_reg_number(2);
        prod_reg_[0 + nb_stages_] = get_max_stage(data[2][0].at(0)); 
    }
    else
    {
        prod_reg_[0] = -1;
        prod_reg_[0 + nb_stages_] = -1;
    }
}

int Mips::check_end(void)
{
    if (cycle_ == MAX_CYCLE)
        return 1;
    for (int i = 0; i < nb_stages_; i++)
    {
        if (stages_[i]->get_inst() != -1)
            return 0;
    }
    return 1;
}

int Mips::get_emul_inst(int stage_nb)
{
    return stages_[stage_nb]->get_emul_inst();
}

int Mips::get_cycle(void)
{
    return cycle_;
}

Program * Mips::get_prog(void)
{
    return prog_;
}

int Mips::check_code(void)
{
    Instruction ** inst = prog_->get_instructions();
    int nb_lines = prog_->get_nb_insts();
    int found = 0;
    int error = 0;
    string inst_name;
    for (int i = 0; i < nb_lines; i++)
    {
        inst_name = op_profile[(inst[i])->getope()].nom;
        found = 0;
        for (int j = 0; j < nb_insts_; j++)
        {
            if (inst_name == Dinst_[j]->get_name())
            {
                found = 1;
                break;
            }
        }
        if (found == 0)
        {
            std::cout << "error : instuction " << inst_name << " is not a valid instruction" << std::endl;
            error = 1;
        }
    }
    return error;
}

int Mips::do_cycle(void)
{
    int end = 0;
    int shift_tab[nb_stages_];
    if (check_code())
        exit(-1);
    if (!check_arch_error())
        exit(-1);
    if (!prog_->check_labels())
        exit(-1);
    while (end != 1)
    {
        for (int i = 0; i < nb_stages_; i++)
            stages_[i]->verif_bypass();
        for (int i = 0; i < nb_stages_; i++)
            stages_[i]->exec();
        for (int i = nb_stages_ - 1; i > -1; i--)
            shift_tab[i] = stages_[i]->shift();
        shift_prod_reg(shift_tab);
        if (cycle_ != 0)
            emul_->inc_cycle();
        cycle_++;
        if (check_end())
            end = 1;
    }
//    emul_->display();
    return 0;
}

