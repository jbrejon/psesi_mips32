
#include <iostream>
#include "EmulProg.h"

EmulProg::EmulProg()
{
    instructions_ = new string[MAX_INST];
    data_ = new string*[MAX_INST];
    for (int i = 0; i < MAX_INST; i++)
        data_[i] = new string[MAX_CYCLE];
    bypass_ = new bypass_t[MAX_BYPASS];
    nb_inst_ = 0;
    nb_cycle_ = 0;
    nb_bypass_ = 0;
}

int  EmulProg::add_inst(string name)
{
    if (nb_inst_ == MAX_INST)
        return -1;
    instructions_[nb_inst_] = name;
    nb_inst_++;
    return 0;
}

void EmulProg::add_data(string dat, int inst_number)
{
    data_[inst_number][nb_cycle_] = dat;
}

int EmulProg::add_bypass(int cycle, int inst1, int inst2, int op)
{
    for (int i = 0; i < nb_bypass_; i++)
    {
        if (bypass_[i].inst1 == inst1 &&
                bypass_[i].inst2 == inst2 &&
                bypass_[i].op    == op)
        {
            bypass_[i].cycle = cycle;
            bypass_[i].inst1 = inst1;
            bypass_[i].inst2 = inst2;
            bypass_[i].op    = op;
            return 1;
        }
    }
    bypass_[nb_bypass_].cycle = cycle;
    bypass_[nb_bypass_].inst1 = inst1;
    bypass_[nb_bypass_].inst2 = inst2;
    bypass_[nb_bypass_].op    = op;
    nb_bypass_++;
    return 0;
}

void EmulProg::inc_cycle(void)
{
    nb_cycle_++;
}

int EmulProg::get_inst(void)
{
    return nb_inst_;
}

int EmulProg::get_cycle(void)
{
    return nb_cycle_;
}

string ** EmulProg::get_data(void)
{
    return data_;
}

int EmulProg::get_nb_bypass(void)
{
    return nb_bypass_;
}

bypass_t * EmulProg::get_bypass(void)
{
    return bypass_;
}

string * EmulProg::get_insts(void)
{
    return instructions_;
}

void EmulProg::display(void)
{
    std::cout << "EMUL || inst : " << nb_inst_ << " || cycles : " << nb_cycle_ << std::endl;
    std::cout << "  \t"; 
    for (int cycle = 1; cycle < nb_cycle_ + 1; cycle++)
        std::cout << cycle; 
    std::cout << std::endl;
    for (int i = 0; i < nb_inst_; i++)
    {
        std::cout << instructions_[i] << "\t";
        for (int j = 0; j < nb_cycle_; j++)
        {
            if (data_[i][j] == "")
                std::cout << " ";
            else
                std::cout << data_[i][j];
        }
        std::cout << std::endl;
    }
    std::cout << "Bypass" << std::endl;
    for (int i = 0; i < nb_bypass_; i++)
        std::cout << "cycle : " << bypass_[i].cycle << " | inst1 : " << bypass_[i].inst1 << " | inst2 : " << bypass_[i].inst2 << " | op : " << bypass_[i].op << std::endl;
}



