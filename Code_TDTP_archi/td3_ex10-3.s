
_strlen:
    Addiu    $29, $29, -1*4
    Addiu    $8, $4 ,  1    ; sauvegarde adresse debut
_strlen_loop:
    Lb       $9 , 0($4)     ; lire 1 caractere (octet)
    Addiu    $4 , $4 , 1    ; caractere suivant
    Bne      $9 , $0 , _strlen_loop    ; fin de chaine ?
    Subu     $2 , $8 , $4    ; calculer nbr de carac

    Addiu    $29, $29, 1*4
    nop
