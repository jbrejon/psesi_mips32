

_strupper:
    Addiu    $29, $29, -1*4

    Addu    $2 , $0 , $4     ; valeur de retour
    Addi    $11, $0 , 'a'    ; pour la comparaison
    Addi    $12, $0 , 'z'    ; pour la comparaison

_strupper_loop:
    Lb      $8 , 0($4)       ; li$e s$c[i]
    Slt     $9 , $8 , $11    ; s$c[i] < 'a'
    Slt     $10, $12, $8     ; 'z'    < s$c[i]
    Or      $10, $10, $9     ; et des 2 conditions
    Bne     $10, $0 , _strupper_endif    ; si 1 des 2 cond vraie
    Addi    $8 , $8 , 'A'-'a'; transformer en majuscule
    Sb      $8 , 0($4)

_strupper_endif:
    Addiu   $4 , $4 , 1      ; caractère suivant
    Bne     $8 , $0 , _strupper_loop

    Addiu   $29, $29, 1*4
    nop
