
_strlen:
    Addiu    $29, $29, -1*4
    Addu     $2, $0 , $0    ; initialisation
_strlen_loop:
    Lb       $9 , 0($4)     ; lire 1 caractere
    Beq      $9 , $0 , _strlen_end_loop    ; fin de chaine ?
    Addiu    $4 , $4 , 1    ; caractere suivant
    Addiu    $2 , $2 , 1    ; caractere suivant
    J        _strlen_loop   ; boucle

_strlen_end_loop:
    Addiu    $29, $29, 1*4
    nop
