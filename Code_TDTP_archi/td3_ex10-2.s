
_strlen:
    Addiu    $29, $29, -1*4
    Addi     $2, $0 , -1    ; initialisation
_strlen_loop:
    Lb       $9 , 0($4)     ; lire 1 caractere
    Addiu    $4 , $4 , 1    ; caractere suivant
    Addi     $2 , $2 , 1    ; caractere suivant
    Bne      $9 , $0 , _strlen_loop    ; fin de chaine ?

    Addiu    $29, $29, 1*4
    nop
