

loop:
    Lw    r8 , 0(r4)        ; lire un élément
    Bgez  r8 , _endif
    Sub   r8 , r0 , r8      ; calculer l'opposé
    nop
    nop
    Sw    r8, 0(r4)

_endif:
    Addiu r4 , r4 , 4
    Bne   r4 , r9 , loop
    nop
    nop
    nop
