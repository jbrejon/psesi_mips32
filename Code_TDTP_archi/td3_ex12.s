

_pgcd :
    Beq     $4 , $5 , _pgcd_end

_pgcd_loop:
    Sltu    $8 , $4 , $5    ; a < b
    Beq     $8 , $0 , _pgcd_else
    Subu    $5 , $5 , $4
    J       _pgcd_endif

_pgcd_else :
    Subu    $4 , $4 , $5
_pgcd_endif:
    Bne     $4 , $5 , _pgcd_loop

_pgcd_end:
    Add     $2 , $4 , $0    ; valeu$ de $etou$
    nop
